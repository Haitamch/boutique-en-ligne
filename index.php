<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <title>Clients</title>
  </head>
  <body>

    <?php
// appel au fichier dbconnect qui
//va assurer la connexion a la base de donnees
include('connectData.php');
$query = "SELECT * FROM clients";
$result = mysqli_query($conn, $query);
//print_r ($result);
 ?>
<div class="container">
   <br>
   <h3>Liste des Clients</h3>
   <br>
 <table class="table table-striped">
   <thead class="thead-dark">
     <tr>
       <th scope="col">id</th>
       <th scope="col">Nom</th>
       <th scope="col">Prenom</th>
        <th scope="col">Ville</th>
        <th scope="col">Email</th>
       <th scope="col">Actions</th>
           </tr>
   </thead>
   <tbody>
 <?php
 while ($row = mysqli_fetch_assoc($result))
//print_r ($row);
 { ?>
    <tr>
       <td scope="row"><?php echo $row['id'] ?></td>
       <td scope="row"><?php echo $row['Nom'] ?></td>
       <td scope="row"><?php echo $row['Prenom'] ?></td>
       <td scope="row"><?php echo $row['Ville'] ?></td>
       <td scope="row"><?php echo $row['Email'] ?></td>
       <td scope="row">

      <a class="btn btn-warning" href="editform.php?id=<?php echo $row['id']; ?>"  role="button"><i class="fas fa-edit"></i></a>

      <a class="btn btn-danger" id="confrim" href="delete.php?id=<?php echo $row['id']; ?>"  role="button"><i class="fas fa-trash"></i></a>



       </td>
    </tr>

 <?php
 }
 mysqli_close($conn);
  ?>


     </tbody>
 </table>
 <a class="btn btn-success" href="addform.php"  role="button">Ajouter</i></a>

 </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
